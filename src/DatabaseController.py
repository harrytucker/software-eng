# from .App import *
from src.model.User import User
from src.model.Exercise import Exercise
from src.model.FoodAndDrink import Meal, Food, Drink
from sqlite3 import *
import sqlite3
import time
import datetime


class DatabaseController:
    def __init__(self):
        # database connection and cursor
        self.db = sqlite3.connect('../models.db')
        self.cursor = self.db.cursor()

    def __delete__(self, instance):
        print('Database control instance shutting down.')

        self.cursor.close()
        self.db.close()

    def timeToSeconds(self, time):
        seconds = 0
        seconds += time.second
        seconds += 60 * time.minute
        seconds += 60 * 60 * time.hour
        return seconds

    def secondsToTime(self, seconds):
        return datetime.time(int(seconds / 3600), int((seconds % 3600) / 60), int((seconds % 3600) % 60))

    def userLoginRequest(self, username, password):
        """ take a username and password, and attempt a login """
        """ return user object if password correct, otherwise return -1 """

        self.cursor.execute('SELECT * FROM Users WHERE username=?', (username,))
        userFound = self.cursor.fetchone()

        if userFound is None:
            print('Username not found!')
            return -1

        # check if returned password matches
        if userFound[7] == password:
            print('Login success!')
            # indexes are out of order because database columns != object attribute order
            userDetails = User(userFound[1],
                               userFound[2],
                               userFound[3],
                               userFound[4],
                               userFound[6],
                               userFound[7])
        else:
            print('Password does not match!')
            return -1

        return userDetails

    def checkForExistingUser(self, username):
        self.cursor.execute('SELECT username FROM Users')

        for row in self.cursor.fetchall():
            for item in row:
                if username == item:
                    print('User already exists!')
                    return -1

        print('Username is fine!')

    def createConnection(db_file):
        """ create a database connection to a SQLite database """
        try:
            conn = sqlite3.connect(db_file)
            print(sqlite3.version)
        except Error as e:
            print(e)
        finally:
            conn.close()

    # USER CRUD METHODS
    # ------------------------------------------------------------------------------------
    def createUser(self, user):
        self.cursor.execute('''INSERT INTO Users(username, email, full_name, weight_kg, BMI, height_m, password)
                          VALUES(?,?,?,?,?,?,?)''', (user.username, user.email, user.fullName, user.weight,
                                                     user.bmi, user.height, user.password))
        self.db.commit()

        print('USER CREATED')

    def fetchAllUsers(self):
        self.cursor.execute('SELECT * FROM Users')

        return self.cursor.fetchall()

    def fetchUser(self, username):
        self.cursor.execute('SELECT * FROM Users WHERE username=?', (username,))

        return self.cursor.fetchall()

    def updateUserWeight(self, username, weight):
        self.cursor.execute('''UPDATE Users
                            SET weight_kg=?
                            WHERE username=?''', (weight, username))

        self.db.commit()
        print('Weight updated.')

    def deleteUser(self, username):
        self.cursor.execute('DELETE FROM Users WHERE username=?', (username,))

        self.db.commit()

    # DRINK CRUD METHODS
    # ------------------------------------------------------------------------------------
    def createDrink(self, name, caloriesPer100ml, user):
        self.cursor.execute('''INSERT INTO Drinks(name, calories_per_100ml)
                            VALUES(?,?)''', (name, caloriesPer100ml))

        self.cursor.execute('SELECT drink_id FROM Drinks WHERE name=?', (name,))
        drink = self.cursor.fetchone()
        drink_id = drink[0]

        self.cursor.execute('SELECT user_id FROM Users WHERE username=?', (user.username,))
        user = self.cursor.fetchone()
        user_id = user[0]

        # add link to user
        self.cursor.execute('INSERT INTO Users_Drinks(user_id, drink_id) VALUES (?, ?)', ((user_id), (drink_id)))

        self.db.commit()

    def fetchAllDrinks(self):
        self.cursor.execute('SELECT * FROM Drinks')

        return self.cursor.fetchall()

    def fetchDrink(self, drinkName):
        self.cursor.execute('SELECT * FROM Drinks WHERE name=?', (drinkName,))

        return self.cursor.fetchall()

    def fetchUserDrinks(self, user):
        self.cursor.execute('SELECT user_id FROM Users WHERE username=?', (user.username,))
        user = self.cursor.fetchone()
        user_id = user[0]

        self.cursor.execute('''SELECT * FROM Drinks
                            INNER JOIN "Users_Drinks" UM on "Drinks".drink_id = UM.drink_id
                            WHERE user_id=?''', (user_id,))

        return self.cursor.fetchall()


    def updateDrink(self):
        print('Not yet implemented.')

    def deleteDrink(self, drinkName):
        self.cursor.execute('DELETE FROM Drinks WHERE name=?', (drinkName))

        self.db.commit()

    # FOOD CRUD METHODS
    # ------------------------------------------------------------------------------------
    def createFood(self, name, caloriesPer100g, user):
        self.cursor.execute('''INSERT INTO Food(name, calories_per_100g)
                            VALUES(?,?)''', (name, caloriesPer100g))

        self.cursor.execute('SELECT food_id FROM Food WHERE name=?', (name,))
        food = self.cursor.fetchone()
        food_id = food[0]

        self.cursor.execute('SELECT user_id FROM Users WHERE username=?', (user.username,))
        user = self.cursor.fetchone()
        user_id = user[0]

        # add link to user
        self.cursor.execute('INSERT INTO Users_Food(user_id, food_id) VALUES (?, ?)', ((user_id), (food_id)))

        self.db.commit()

    def fetchAllFood(self):
        self.cursor.execute('SELECT * FROM Food')

        return self.cursor.fetchall()

    def fetchUserFoods(self, user):
        self.cursor.execute('SELECT user_id FROM Users WHERE username=?', (user.username,))
        user = self.cursor.fetchone()
        user_id = user[0]

        self.cursor.execute('''SELECT * FROM Food
                            INNER JOIN "Users_Food" UM on "Food".food_id = UM.food_id
                            WHERE user_id=?''', (user_id,))

        return self.cursor.fetchall()

    def fetchFood(self, food):
        self.cursor.execute('SELECT * FROM Food WHERE name=?', (food.name,))

    # MEAL CRUD METHODS
    # ------------------------------------------------------------------------------------
    def addNewMeal(self, meal, user):
        if meal.food is None:
            food_id = None
        else:
            """ fetch food id  """
            self.cursor.execute('SELECT food_id FROM Food WHERE name=?', (meal.food.name,))
            food = self.cursor.fetchone()
            food_id = food[0]

        if meal.drink is None:
            drink_id = None
        else:
            """ fetch drink id  """
            self.cursor.execute('SELECT drink_id FROM Drinks WHERE name=?', (meal.drink.name,))
            drink = self.cursor.fetchone()
            drink_id = drink[0]

        # add to the meal table
        self.cursor.execute('''INSERT INTO Meal(date_added, meal_of_the_day, food_id, 
                                          food_amount, drink_id, drink_amount, calories) VALUES(?, ?, ?, ?, ?, ?, ?)''',
                            (time.mktime(datetime.datetime.combine(meal.dateAdded, meal.timeAdded).timetuple()), meal.mealOfTheDay, food_id, meal.foodAmount,
                             drink_id, meal.drinkAmount, meal.getCalories()))

        self.cursor.execute('SELECT MAX(meal_id) from Meal')
        meal = self.cursor.fetchone()
        meal_id = meal[0]
        self.db.commit()

        self.cursor.execute('SELECT user_id FROM Users WHERE username=?', (user.username,))
        user = self.cursor.fetchone()
        user_id = user[0]


        # add link to user
        self.cursor.execute('INSERT INTO Users_Meals(user_id, meal_id) VALUES (?, ?)', (user_id, meal_id))
        self.db.commit()

    def fetchUsersMeals(self, user):
        self.cursor.execute('SELECT user_id FROM Users WHERE username=?', (user.username,))
        user = self.cursor.fetchone()
        user_id = user[0]

        self.cursor.execute('''SELECT * FROM Meal
                            INNER JOIN "Users_Meals" UM on "Meal".meal_id = UM.meal_id
                            WHERE user_id=?''', (user_id,))

        return self.cursor.fetchall()

    def fetchAllMeals(self):
        self.cursor.execute('SELECT * FROM Meal')

        return self.cursor.fetchall()

    def fetchUserMeals(self, user):
        self.cursor.execute('SELECT * FROM Meal WHERE meal_id IN (SELECT meal_id FROM Users_Meals WHERE user_id IN (SELECT user_id FROM Users WHERE username=?))', (user.getUsername(),))

        fetch = self.cursor.fetchall()
        meals = []
        for m in fetch:
            self.cursor.execute('SELECT name, calories_per_100g FROM Food WHERE food_id=?', (m[3],))
            foodDetails = self.cursor.fetchone()
            self.cursor.execute('SELECT name, calories_per_100ml FROM Drinks WHERE drink_id=?', (m[5],))
            drinkDetails = self.cursor.fetchone()

            food = None
            drink = None
            if foodDetails is not None:
                food = Food(foodDetails[0], foodDetails[1])
            if drinkDetails is not None:
                drink = Drink(drinkDetails[0], drinkDetails[1])
            timeAdded = datetime.datetime.fromtimestamp(m[1])
            meals.append(Meal(timeAdded.date(), timeAdded.time(), m[2], food, m[4], drink, m[6]))

        return meals

    def fetchMeal(self, meal_name):
        self.cursor.execute('SELECT * FROM Meal WHERE name=?', (meal_name,))

        return self.cursor.fetchall()

    def updateMeal(self, meal):
        self.cursor.execute('''UPDATE Meal
                            SET calories=?
                            WHERE meal_id=?''', (meal.calories, meal.id))

    def deleteMeal(self, meal_id):
        self.cursor.execute('DELETE FROM Meal WHERE meal_id=?', (meal_id,))

    # GOAL CRUD METHODS
    # ------------------------------------------------------------------------------------
    def creatGoal(self, goal):
        self.cursor.execute('''INSERT INTO Goals(goalName, target_date, is_finished, is_met, date_added)
                          VALUES(?,?,?,?,?)''', (goal.goalName, goal.targetDate, goal.finished, goal.isMet,
                                                 goal.startDate))

        self.db.commit()

    def fetchAllGoals(self):
        self.cursor.execute('SELECT * FROM Goals')

        return self.cursor.fetchall()

    def fetchUserGoals(self, user):
        self.cursor.execute('SELECT * FROM Goals WHERE goal_id IN (SELECT goal_id FROM Users_Goals WHERE user_id IN (SELECT user_id FROM Users WHERE username=?))', (user.getUsername(),))

        return self.cursor.fetchall()

    def fetchOngoingGoals(self):
        self.cursor.execute('SELECT * FROM Goals WHERE is_finished="FALSE" AND is_met="FALSE"')

        return self.cursor.fetchall()

    def fetchAllFinishedGoals(self):
        self.cursor.execute('SELECT * FROM Goals WHERE is_finished="TRUE" OR is_met="TRUE"')

        return self.cursor.fetchall()

    def fetchGoal(self, goal_name):
        self.cursor.execute('SELECT * FROM Goals WHERE name=?', (goal_name,))

        return self.cursor.fetchall()

    def updateGoal(self, goalName):
        print("Not yet implemented.")

    def deleteGoal(self, goalName):
        self.cursor.execute('DELETE FROM Goals WHERE goalName=?', (goalName))

        self.db.commit()

    # GROUP CRUD METHODS
    # ------------------------------------------------------------------------------------
    def createGroup(self, group):
        self.cursor.execute('INSERT INTO Groups(name) VALUES(?)', (group.name,))

    def fetchUserGroups(self, user):
        self.cursor.execute('SELECT * FROM Groups WHERE group_id IN (SELECT group_id FROM Users_Groups WHERE user_id IN (SELECT user_id FROM Users WHERE username=?))', (user.getUsername(),))

        return self.cursor.fetchall()

    def fetchAllGroups(self):
        self.cursor.execute('SELECT * FROM Groups')

        return self.cursor.fetchall()

    def fetchGroup(self, group_name):
        self.cursor.execute('SELECT * FROM Groups WHERE name=?', (group_name,))

        return self.cursor.fetchall()

    def updateGroup(self, group):
        self.cursor.execute('''UPDATE Groups
                            SET name=?
                            WHERE name=?''', (group.name, group.name))

        self.db.commit()
        print("not implemented yet")

    def deleteGroup(self, group):
        self.cursor.execute('DELETE FROM Groups WHERE name=?', (group.name,))

        self.db.commit()
        return self.cursor.fetchone()

    # EXERCISE CRUD METHODS
    # ------------------------------------------------------------------------------------
    def createExercise(self, exercise, user):
        self.cursor.execute('''INSERT INTO Exercise(title, 
                                            date_completed, 
                                            time_completed, 
                                            type_of_exercise, 
                                            duration, 
                                            distance_meters) 
                                            VALUES(?,?,?,?,?,?)''',
                            (exercise.title,
                             time.mktime(exercise.dateAdded.timetuple()),
                             self.timeToSeconds(exercise.timeAdded),
                             exercise.typeOfExercise,
                             self.timeToSeconds(exercise.duration),
                             exercise.distance))

        self.cursor.execute('SELECT MAX(exercise_id) from Exercise')
        exercise = self.cursor.fetchone()
        exercise_id = exercise[0]
        self.db.commit()

        self.cursor.execute('SELECT user_id FROM Users WHERE username=?', (user.username,))
        user = self.cursor.fetchone()
        user_id = user[0]


        # add link to user
        self.cursor.execute('INSERT INTO Users_Exercise(user_id, exercise_id) VALUES (?, ?)', (user_id, exercise_id))
        self.db.commit()

    def fetchAllExercise(self):
        self.cursor.execute('SELECT * FROM Exercise')

        return self.cursor.fetchall()

    def fetchExercise(self, title):
        self.cursor.execute('SELECT * FROM Exercise WHERE title=?', (title,))

        return self.cursor.fetchall()

    def fetchUserExercise(self, user):
        self.cursor.execute('SELECT user_id FROM Users WHERE username=?', (user.username,))
        user = self.cursor.fetchone()
        user_id = user[0]

        self.cursor.execute('''SELECT * FROM Exercise
                            INNER JOIN "Users_Exercise" UM on "Exercise".exercise_id = UM.exercise_id
                            WHERE user_id=?''', (user_id,))

        fetch = self.cursor.fetchall()

        exercises = []
        for e in fetch:
            dateAdded = datetime.date.fromtimestamp(e[2])
            timeAdded = self.secondsToTime(e[3])
            duration = self.secondsToTime(e[5])
            exercise = Exercise(e[1], dateAdded, timeAdded,e[4], datetime.time(duration.hour, duration.minute, duration.second), e[6])
            exercises.append(exercise)

        return exercises

    def fetchGoalExercise(self, goalName):
        self.cursor.execute('SELECT * FROM Exercise WHERE exercise_id IN (SELECT exercise_type FROM Goals WHERE name=?)', (goalName,))

        return self.cursor.fetchall()

    def updateExercise(self):
        print('not yet implemented')

    def deleteExercise(self, title):
        self.cursor.execute('DELETE FROM Exercise WHERE title=?', (title,))

        self.db.commit()


if __name__ == '__main__':
    """ TEST HARNESS """
    controller = DatabaseController()

    #print('CREATE USER:')
    #controller.createUser("johndoe", "j.doe@uea.ac.uk", "John Doe", 130, 22, 2, "password")

    #print('ALL USERS:')
    #print(controller.fetchAllUsers())

    #print('SPECIFIC USERS:')
    #print(controller.fetchUser('johndoe'))

    test = controller.userLoginRequest('mrtrump', 'solid')
    print(str(test))
    # close database connection
