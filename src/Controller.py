from src.App import *
from src.model.Goal import Goal

class Controller:
    def __init__(self, args):
        self.app = QApplication(args)
        self.screenController = ScreenController(self)
        self.databaseController = DatabaseController()
        self.screenController.show()
        self.userLoggedIn = None

    def sendLoginRequest(self, username, password):
        return self.databaseController.userLoginRequest(username, password)

    def addNewUser(self, newUser):
        self.databaseController.createUser(newUser)
        for food in User.DEFAULT_USER_FOODS:
            self.databaseController.createFood(food.name, food.caloriesPer100g, newUser)
        for drink in User.DEFAULT_USER_DRINKS:
            self.databaseController.createDrink(drink.name, drink.caloriesPer100ml, newUser)

    def getUserFullName(self):
        return self.userLoggedIn.fullName

    def setUserLoggedIn(self, user):
        self.userLoggedIn = user

    def getUserLoggedIn(self):
        return self.userLoggedIn

    def loadUserInfo(self):
        self.setUserGroups()
        self.setUserGoals()
        self.setUserMeals()
        self.setUserExercises()

    def createNewExerciseGoal(self, goal):
        self.userLoggedIn.addGoal(goal)
        print("new goal")
        #self.databaseController.addNewExerciseGoal(goal)

    def recordWeight(self, weight):
        self.userLoggedIn.weight = weight
        self.userLoggedIn.bmi = self.userLoggedIn.calculateBmi()
        self.updateGoalsWithWeight(weight)
        #self.databaseController.recordWeightAndBmi(weight, self.userLoggedIn.bmi)

    def updateGoal(self, goal):
        print("goal updated")

    def checkGoalsExpire(self):
        for goal in self.userLoggedIn.getGoals():
            if not goal.finished:
                if goal.targetDate >= datetime.datetime.now():
                    goal.finished = True
                    self.updateGoal()
                    self.showPopup("Goal has expired")

    def updateGoalsWithExercise(self, exercise):
        for goal in self.userLoggedIn.goals:
            if type(goal) == "ExerciseGoal" and not goal.finished:
                if goal.exerciseType == exercise.typeOfExercise:
                    goal.distanceMoved += exercise.distance
                    goal.durationExercised += exercise.duration

                    if goal.distance is not None:
                        if goal.distance <= goal.distanceMoved:
                            goal.finished = True
                            goal.isMet = True
                            self.updateGoal()
                            self.showPopup("Goal has been met")
                    elif goal.duration is not None:
                        if goal.duration <= goal.durationExercised:
                            goal.finished = True
                            goal.isMet = True
                            self.updateGoal()
                            self.showPopup("Goal has been met")

    def updateGoalsWithWeight(self, weight):
        for goal in self.userLoggedIn.goals:
            if type(goal) == "WeightGoal" and not goal.finished:
                if goal.needToGainWeight and weight > goal.weight:
                    goal.finished = True
                    goal.isMet = True
                    self.updateGoal()
                    self.showPopup("Goal has been met")
                if not goal.needToGainWeight and weight < goal.weight:
                    goal.finished = True
                    goal.isMet = True
                    self.updateGoal()
                    self.showPopup("Goal has been met")

    def showPopup(self, message):
        self.screenController.showPopup(message)

    def logout(self):
        self.userLoggedIn = None

    def getMeal(self, meal_name):
        return self.databaseController.fetchMeal(meal_name)

    def getUserMeals(self):
        return self.userLoggedIn.getMeals()

    def setUserMeals(self):
        foods = self.databaseController.fetchUserFoods(self.userLoggedIn)
        drinks = self.databaseController.fetchUserDrinks(self.userLoggedIn)
        self.userLoggedIn.foods = []
        for food in foods:
            self.userLoggedIn.foods.append(Food(food[1], food[2]))
        self.userLoggedIn.drinks = []
        for drink in drinks:
            self.userLoggedIn.drinks.append(Drink(drink[1], drink[2]))
        self.userLoggedIn.setMeals(self.databaseController.fetchUserMeals(self.userLoggedIn))

    def getExercise(self, index):
        return self.userLoggedIn.exercises[index]

    def getUserExercises(self):
        return self.databaseController.fetchUserExercise(self.userLoggedIn)

    def setUserExercises(self):
        self.userLoggedIn.setExercises(self.databaseController.fetchUserExercise(self.userLoggedIn))

    def getUserGroups(self):
        return self.userLoggedIn.getGroups()

    def setUserGroups(self):
        self.userLoggedIn.setGroups(self.databaseController.fetchUserGroups(self.userLoggedIn))

    def getGroups(self):
        return self.databaseController.fetchAllGroups()

    def getGroup(self, group_name):
        return self.databaseController.fetchGroup(group_name)

    def getGoal(self, goal_name):
        return self.databaseController.fetchGoal(goal_name)

    def getOngoingGoals(self):
        return self.databaseController.fetchOngoingGoals()

    def getUserGoals(self):
        return self.userLoggedIn.getGoals()

    def setUserGoals(self):
        self.userLoggedIn.setGoals(self.databaseController.fetchUserGoals(self.userLoggedIn))

    def getFinishedGoals(self):
        return self.databaseController.fetchAllFinishedGoals()

    def sortFoodAndExercise(self):
        self.userLoggedIn.sortFoodAndExercise

    def addNewFood(self, name, caloriesPer100g):
        self.userLoggedIn.addFood(Food(name, caloriesPer100g))
        self.databaseController.createFood(name, caloriesPer100g, self.userLoggedIn)
        print("made food")

    def addNewDrink(self, name, caloriesPer100ml):
        self.userLoggedIn.addDrink(Drink(name, caloriesPer100ml))
        self.databaseController.createDrink(name, caloriesPer100ml, self.userLoggedIn)
        print("made drink")

    def addNewExercise(self, title, dateAdded, timeAdded, type, duration, distance):
        exercise = Exercise(title, dateAdded, timeAdded, type, duration, distance)
        self.userLoggedIn.addExercise(exercise)
        self.databaseController.createExercise(exercise, self.userLoggedIn)
        self.updateGoalsWithExercise(exercise)
        print("made exercise")

    def addNewMeal(self, dateAdded, timeAdded, mealOfTheDay, food, foodAmount, drink, drinkAmount):
        meal = Meal(dateAdded, timeAdded, mealOfTheDay, food, foodAmount, drink, drinkAmount)
        self.userLoggedIn.addMeal(meal)
        self.databaseController.addNewMeal(meal, self.userLoggedIn)
        print("meal added")

    #Tucker to implement this in db controller
    #def addNewMeal(self, meal):
    #    self.databaseController.createMeal(meal)