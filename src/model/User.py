from .FoodAndDrink import Food, Drink
from .Exercise import Exercise, ExerciseType

class User:
    DEFAULT_USER_FOODS = [Food("apple", 50),
                          Food("lasagna", 300),
                          Food("protein shake", 200),
                          Food("chicken", 250),
                          Food("biscuits", 400),
                          Food("banana", 100),
                          Food("lettuce", 20),
                          Food("beef", 300),
                          Food("grass", 100),
                          Food("cheese", 400)]
    DEFAULT_USER_DRINKS = [Drink("water", 0),
                           Drink("orange juice", 150),
                           Drink("apple juice", 150),
                           Drink("grape juice", 150),
                           Drink("cola", 150),
                           Drink("fanta",130),
                           Drink("whole milk", 150),
                           Drink("skimmed milk", 50),
                           Drink("gravy", 100),
                           Drink("red bull", 200)]
    DEFAULT_USER_EXERCISES = [ExerciseType("Running", True, True),
                              ExerciseType("Swimming", True, True),
                              ExerciseType("Tennis", False, True),
                              ExerciseType("Football", False, True),
                              ExerciseType("Weight lifting", False, True),
                              ExerciseType("Climbing", False, True),
                              ExerciseType("Yoga", False, True),
                              ExerciseType("Rowing", True, True),
                              ExerciseType("Cycling", True, True),
                              ExerciseType("Hiking", True, True)]

    def __init__(self, username, email, fullName, weight, height, password, foods = [], drinks = [], groups = [], goals = [], exercises = [], meals = []):
        self.username = username
        self.email = email
        self.fullName = fullName
        self.weight = weight
        self.height = height
        self.password = password
        self.bmi = self.calculateBmi()
        self.foods = foods
        self.drinks = drinks
        self.groups = groups
        self.goals = goals
        self.exercises = exercises
        self.meals = meals
        self.addDefaultValues()
        self.dbController = DatabaseController()

    def calculateBmi(self):
        return self.weight / self.height ** 2

    def addFood(self, food):
        self.foods.append(food)

    def addDrink(self, drink):
        self.drinks.append(drink)

    def addExercise(self, exercise):
        self.exercises.append(exercise)

    def addMeal(self, meal):
        self.meals.append(meal)

    def addGoal(self, goal):
        self.goals.append(goal)

    def addToGroup(self, groupId):
        self.groups.append(groupId)

    def removeFromGroup(self, groupId):
        self.groups.remove(groupId)

    def addDefaultValues(self):
        self.foods += self.DEFAULT_USER_FOODS
        self.drinks += self.DEFAULT_USER_DRINKS

    def sortFoodAndExercise(self):
        self.meals.sort()
        self.exercises.sort()

    def getUsername(self):
        return self.username

    def getEmail(self):
        return self.email

    def getFullName(self):
        return self.fullName

    def getMeals(self):  # TODO
        return self.dbController.fetchUsersMeals(self)

    def getWeight(self):
        return self.weight

    def getBMI(self):
        return self.bmi

    def getHeight(self):
        return self.height

    def setMeals(self, meals):
        self.meals = meals

    def getExerciseTypes(self):
        return self.DEFAULT_USER_EXERCISES

    def getExercises(self):
        return self.exercises

    def setExercises(self, exercises):
        self.exercises = exercises

    def getGroups(self):
        return self.groups

    def setGroups(self, groups):
        self.groups = groups

    def getGoals(self):
        return self.goals

    def setGoals(self, goals):
        self.goals = goals


from src.DatabaseController import DatabaseController