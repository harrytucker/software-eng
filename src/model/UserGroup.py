class UserGroup:
    def __init__(self, groupId, users, name, goals):
        self.groupId = groupId
        self.users = users
        self.name = name
        self.ongoingGoals = []
        self.completedGoals = []

        for goal in goals:
            if goal.finished:
                self.completedGoals.append(goal)
            else:
                self.ongoingGoals.append(goal)

    def getGroupName(self):
        return self.name

    def addUser(self, username):
        self.users.append(username)

    def removeUser(self, username):
        self.users.remove(username)

    def addGoal(self, goal):
        self.ongoingGoals.append(goal)

    def checkIfGoalsComplete(self):
        for goal in self.ongoingGoals:
            if goal.checkIfGoalExpired(goal):
                goal.checkIfGoalMet(goal)
                self.completedGoals.append(goal)
                self.ongoingGoals.remove(goal)

    def getGroupId(self):
        return self.groupId

    def getUsers(self):
        return self.users

    def getOngoingGoals(self):
        return self.ongoingGoals

    def getCompletedGoals(self):
        return self.completedGoals