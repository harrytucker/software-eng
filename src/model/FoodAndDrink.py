from enum import Enum
import datetime

class Food:
    def __init__(self, name, calories):
        self.name = name
        self.caloriesPer100g = calories

    def __str__(self):
        return self.name + " with " + str(self.caloriesPer100g) + "kcal per 100g"


class Drink:
    def __init__(self, name, calories):
        self.name = name
        self.caloriesPer100ml = calories

    def __str__(self):
        return self.name + " with " + str(self.caloriesPer100ml) + "kcal per 100ml"


class MealType(Enum):
    Breakfast = 0
    Lunch = 1
    Snack = 2
    Dinner = 3


class Meal:
    def __init__(self, dateAdded, timeAdded, mealOfTheDay, food, foodAmount, drink, drinkAmount):
        self.dateAdded = dateAdded
        self.timeAdded = timeAdded
        self.mealOfTheDay = mealOfTheDay
        self.food = food
        self.foodAmount = foodAmount
        self.drink = drink
        self.drinkAmount = drinkAmount
        self.calories = self.getCalories()

    def getCalories(self):
        c = 0
        if self.food is not None:
            c += int(self.food.caloriesPer100g * self.foodAmount / 100)
        if self.drink is not None:
            c += int(self.drink.caloriesPer100ml * self.drinkAmount / 100)
        return c

    def getDateAdded(self):
        return self.dateAdded

    def getTimeAdded(self):
        return self.timeAdded

    def getMealOfTheDay(self):
        return self.mealOfTheDay

    def getFood(self):
        return self.food

    def getFoodAmount(self):
        return self.foodAmount

    def getDrink(self):
        return self.drink

    def getDrinkAmount(self):
        return self.drinkAmount

    def __str__(self):
        return MealType(self.mealOfTheDay).name + " at " + str(self.timeAdded)

    def __gt__(self, other):
        if self.dateAdded > other.dateAdded:
            return True
        elif self.timeAdded > other.timeAdded and self.dateAdded == other.dateAdded:
            return True
        return False

    def __lt__(self, other):
        if self.dateAdded < other.dateAdded:
            return True
        elif self.timeAdded < other.timeAdded and self.dateAdded == other.dateAdded:
            return True
        return False
