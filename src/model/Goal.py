from src.App import *
import datetime

class Goal:
    def __init__(self, startDate, targetDate, goalName, finished, isMet):
        self.startDate = startDate
        self.targetDate = targetDate
        self.name = goalName
        self.finished = finished
        self.isMet = isMet

    def __str__(self):
        return self.name

    def checkIfGoalExpired(self):
        #If date now is same as or greater than target date then goal is expired
        if datetime.date.now() >= self.targetDate:
            self.finished = True
            return True
        return False

    def checkIfGoalMet(self):
        pass

    def getTargetDate(self):
        return self.targetDate

class ExerciseGoal(Goal):
    def __init__(self, startDate, targetDate, goalName, finished, isMet, exerciseType, duration, distance):
        super().__init__(startDate, targetDate, goalName, finished, isMet)
        self.exerciseType = exerciseType
        self.duration = duration
        self.distance = distance
        self.distanceMoved = 0
        self.durationExercised = datetime.time(0, 0, 0)

    def checkIfGoalMet(self, distance, duration):
        print("yes")
        #If distance goal set and distance is less than target return false
        if self.distance > distance and self.distance is not None:
            return False
        #If duration goal set and duration less than target return false
        if self.duration > duration and self.distance is not None:
            return False
        #Else return true
        self.isMet = True
        return True

class WeightGoal(Goal):
    def __init__(self, startDate, targetDate, goalName, finished, isMet, weight, needToGainWeight):
        super.__init__(self, startDate, targetDate, goalName, finished, isMet)
        self.weight = weight
        #If user needs to gain weight above target or lose weight below target
        self.needToGainWeight = needToGainWeight

    def checkIfGoalMet(self, weight):
        #If user weight is greater than target weight and target is to gain weight return true
        if self.weight < weight and self.needToGainWeight:
            return True
        #Else if user weight is less than target weight and target is to lose weight return true
        elif self.weight > weight and not self.needToGainWeight:
            return True
        #else return false
        self.isMet = True
        return False