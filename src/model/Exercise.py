import datetime

#Used to define list of exercises within the application
#Some exercises use distance, some use duration and some use both as a measure of level of exercise
class ExerciseType:
    def __init__(self, name, hasDistance, hasDuration):
        self.name = name
        self.hasDistance = hasDistance
        self.hasDuration = hasDuration

    def __str__(self):
        return self.name

#Exercise is used to record activities that user completes
class Exercise:
    def __init__(self, title, dateAdded, timeAdded, typeOfExercise, duration, distance):
        self.title = title
        self.dateAdded = dateAdded
        self.timeAdded = timeAdded
        self.typeOfExercise = typeOfExercise
        self.duration = duration
        self.distance = distance

    def getTitle(self):
        return self.title

    def getDate(self):
        return self.dateAdded

    def getTime(self):
        return self.timeAdded

    def getType(self):
        return self.typeOfExercise

    def getDuration(self):
        return self.duration

    def getDistamce(self):
        return self.distance

    def __str__(self):
        return self.title

    def __gt__(self, other):
        if self.dateAdded > other.dateAdded:
            return True
        elif self.timeAdded > other.timeAdded and self.dateAdded == other.dateAdded:
            return True
        return False

    def __lt__(self, other):
        if self.dateAdded < other.dateAdded:
            return True
        elif self.timeAdded < other.timeAdded and self.dateAdded == other.dateAdded:
            return True
        return False
