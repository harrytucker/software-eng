from src.App import *

class ScreenController(QMainWindow):
    def __init__(self, controller):
        super().__init__()
        self.screenStack = ["login"]
        self.controller = controller
        self.setWindowTitle('Toonk fitness')
        self.mainWidget = None
        self.popup = None
        self.mainScreenTab = 0

    #Get current screen and render it
    #screenStack[-1] is last element in screenStack
    def show(self):
        if self.screenStack[-1] == "login":
            self.mainWidget = LoginScreen(self, self.controller)
            self.setCentralWidget(self.mainWidget)
        elif self.screenStack[-1] == "main":
            self.mainWidget = MainScreen(self, self.controller, self.mainScreenTab)
            self.setCentralWidget(self.mainWidget)
        elif self.screenStack[-1] == "newUser":
            self.mainWidget = NewUserScreen(self, self.controller)
            self.setCentralWidget(self.mainWidget)
        else:
            print("Screen " + self.screenStack[-1] + " doesn't exist")
        super().show()

    #Return to previous screen
    #If screen stack only contains one screen then return to mainScreen
    def changeToPreviousScreen(self):
        if len(self.screenStack) > 1:
            self.screenStack.pop()
        else:
            self.screenStack = ["mainScreen"]
        self.show()

    #Move to new screen retaining stack navigation
    def addToScreenStack(self, screen):
        self.screenStack.append(screen)
        self.show()

    #Forget all stack navigation and set current screen to new screen
    def setCurrentScreen(self, screen):
        self.screenStack = [screen]
        self.show()

    def showPopup(self, message):
        self.popup = QDialog()
        self.popup.setWindowTitle(message)

        self.popup.layout = QVBoxLayout()
        self.popup.setLayout(self.popup.layout)

        self.popup.closeButton = QPushButton('Ok')
        self.popup.closeButton.clicked.connect(self.closePopup)
        self.popup.layout.addWidget(self.popup.closeButton)

        self.popup.setWindowModality(Qt.ApplicationModal)
        self.popup.exec_()

    def closePopup(self):
        self.popup.close()
        self.updateScreen()

    #Refreshes screen
    def updateScreen(self):
        self.show()
