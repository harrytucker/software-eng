from src.App import *
from src.screens.NewMeal import NewMeal, AddNewFood, AddNewDrink
from src.screens.NewGoal import *
from src.screens.NewExercise import NewExercise
from src.screens.NewWeight import NewWeight
from src.model.FoodAndDrink import MealType
from datetime import datetime

class MainScreen(QWidget):
    def __init__(self, parent, controller, startingTab = 0):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.layout = QVBoxLayout()

        self.mainTab = ""
        self.mainInfo = "Info here:"
        self.mainTextBox = ""
        self.goalTab = ""
        self.goalInfo = "Goal info here:"
        self.goalTextBox = ""
        self.groupTab = ""
        self.groupInfo = "Group info here:"
        self.groupTextBox = ""

        self.tabs = QTabWidget()
        self.homeTab = self.createHomeTab()
        self.goalsTab = self.createGoalsTab()
        self.groupsTab = self.createGroupsTab()
        self.tabs.addTab(self.homeTab, "Home")
        self.tabs.addTab(self.goalsTab, "Goals")
        self.tabs.addTab(self.groupsTab, "Groups")
        self.tabs.setCurrentIndex(startingTab)
        self.tabs.currentChanged.connect(self.changeTab)
        self.layout.addWidget(self.tabs)

        self.popup = None

        self.setLayout(self.layout)

    def changeTab(self, i):
        self.parent.mainScreenTab = i

    def logout(self):
        self.controller.logout()
        self.parent.setCurrentScreen("login")

    def addMeal(self):
        self.popup = QDialog()
        self.popup.setWindowTitle("Add new meal")

        self.popup.layout = QVBoxLayout()
        self.popup.setLayout(self.popup.layout)
        self.popup.mainWidget = NewMeal(self, self.controller)
        self.popup.layout.addWidget(self.popup.mainWidget)
        self.popup.setWindowModality(Qt.ApplicationModal)
        self.popup.exec_()

    def recordWeight(self):
        self.popup = QDialog()
        self.popup.setWindowTitle("Record new weight")

        self.popup.layout = QVBoxLayout()
        self.popup.setLayout(self.popup.layout)
        self.popup.mainWidget = NewWeight(self, self.controller)
        self.popup.layout.addWidget(self.popup.mainWidget)
        self.popup.setWindowModality(Qt.ApplicationModal)
        self.popup.exec_()

    def addGoal(self):
        self.popup = QDialog()
        self.popup.setWindowTitle("Create new goal")

        self.popup.layout = QVBoxLayout()
        self.popup.setLayout(self.popup.layout)
        self.popup.mainWidget = NewGoal(self, self.controller)
        self.popup.layout.addWidget(self.popup.mainWidget)
        self.popup.setWindowModality(Qt.ApplicationModal)
        self.popup.exec_()

    def changePopupToAddMeal(self):
        self.popup.layout.removeWidget(self.popup.mainWidget)
        self.popup.mainWidget.close()

        self.popup.mainWidget = NewMeal(self, self.controller)
        self.popup.layout.addWidget(self.popup.mainWidget)

    def changePopupToAddFood(self):
        self.popup.layout.removeWidget(self.popup.mainWidget)
        self.popup.mainWidget.close()
        self.popup.mainWidget = AddNewFood(self, self.controller)
        self.popup.layout.addWidget(self.popup.mainWidget)

    def changePopupToAddDrink(self):
        self.popup.layout.removeWidget(self.popup.mainWidget)
        self.popup.mainWidget.close()

        self.popup.mainWidget = AddNewDrink(self, self.controller)
        self.popup.layout.addWidget(self.popup.mainWidget)

    def confirmOnPopupClose(self, message):
        self.popup.layout.removeWidget(self.popup.mainWidget)
        self.popup.mainWidget.close()

        self.popup.mainWidget = QWidget()
        self.popup.mainWidget.layout = QVBoxLayout()
        self.popup.mainWidget.setLayout(self.popup.mainWidget.layout)
        self.popup.setWindowTitle(message)
        self.popup.mainWidget.closeButton = QPushButton('Ok')
        self.popup.mainWidget.closeButton.clicked.connect(self.closePopup)
        self.popup.mainWidget.layout.addWidget(self.popup.mainWidget.closeButton)

        self.popup.layout.addWidget(self.popup.mainWidget)

    def closePopup(self):
        self.popup.close()
        self.parent.updateScreen()

    def addExercise(self):
        self.popup = QDialog()
        self.popup.setWindowTitle("Add new exercise")

        self.popup.layout = QVBoxLayout()
        self.popup.setLayout(self.popup.layout)
        self.popup.mainWidget = NewExercise(self, self.controller)
        self.popup.layout.addWidget(self.popup.mainWidget)
        self.popup.setWindowModality(Qt.ApplicationModal)
        self.popup.exec_()

    def mealItemActivated_event(self, item):
        index = self.homeTab.content.mealListWidget.mealList.currentRow()
        self.mainText = self.controller.userLoggedIn.meals[index]

        self.mainInfo = (str(self.mainText) + "\n\n" +
                         "Date Added: " + str(self.mainText.dateAdded) + "\n\n")
        if self.mainText.food is not None:
            self.mainInfo += "Food: " + str(self.mainText.foodAmount) + "g of " + str(self.mainText.food) + "\n\n"
        if self.mainText.drink is not None:
            self.mainInfo += "Drink: " + str(self.mainText.drinkAmount) + "ml of " + str(self.mainText.drink) + "\n\n"
        self.mainInfo += "Calories: " + str(self.mainText.calories)

        self.loadMainInfoBox(self.mainTab, self.mainTextBox)

    def exerciseItemActivated_event(self, item):
        index = self.homeTab.content.exerciseListWidget.exerciseList.currentRow()
        self.mainText = self.controller.getExercise(index)
        self.mainInfo = ("Title: " + str(self.mainText) + "\n\n" +
                         "Exercise Type: " + str(self.mainText.typeOfExercise) + "\n\n")
        if self.mainText.duration  is not None:
            self.mainInfo += "Duration: " + str(self.mainText.duration) + "\n\n"
        if self.mainText.distance is not None:
            self.mainInfo += "Distance: " + str(int(self.mainText.distance)) + "m\n\n"
        if self.mainText.dateAdded is not None:
            self.mainInfo += "Date Completed: " + str(datetime.combine(self.mainText.dateAdded, self.mainText.timeAdded))
        self.loadMainInfoBox(self.mainTab, self.mainTextBox)

    def loadMainInfoBox(self, tab, textBox):
        textBox.setPlainText(str(self.mainInfo))
        textBox.setReadOnly(True)
        tab.layout.addWidget(textBox)

    def createHomeTab(self):
        tab = QWidget()
        tab.layout = QVBoxLayout(self)
        tab.setLayout(tab.layout)

        #Add title widget
        tab.title = QLabel()
        tab.title.setText("Welcome " + self.controller.getUserFullName())
        tab.stats = QLabel()
        tab.stats.setText("Height: " + str(self.controller.userLoggedIn.height) + "m\n" +
                          "Weight: " + str(self.controller.userLoggedIn.weight) + "kg\n" +
                          "BMI: " + str(round(self.controller.userLoggedIn.bmi, 2)))

        # generate an advisory message based off BMI value of logged in user
        tab.advisory = QLabel()
        if self.controller.userLoggedIn.bmi < 18.5:
            tab.advisory.setText("Under weight")
        elif self.controller.userLoggedIn.bmi < 24.9:
            tab.advisory.setText("Normal weight")
        elif self.controller.userLoggedIn.bmi < 29.9:
            tab.advisory.setText("Over weight")
        elif self.controller.userLoggedIn.bmi < 34.9:
            tab.advisory.setText("Obesity Class-I")
        elif self.controller.userLoggedIn.bmi < 39.9:
            tab.advisory.setText("Obesity Class-II")
        else:
            tab.advisory.setText("Obesity Class-III")

        tab.layout.addWidget(tab.title)
        tab.layout.addWidget(tab.stats)
        tab.layout.addWidget(tab.advisory)

        #Create horizontal box widget and layout
        tab.content = QWidget()
        tab.layout.addWidget(tab.content)
        tab.content.layout = QHBoxLayout(tab.content)
        tab.content.setLayout(tab.content.layout)

        #Vertically aligned list of meals with label
        tab.content.mealListWidget = QWidget()
        tab.content.layout.addWidget(tab.content.mealListWidget)
        tab.content.mealListWidget.layout = QVBoxLayout(tab.content.mealListWidget)
        tab.content.mealListWidget.setLayout(tab.content.mealListWidget.layout)

        tab.content.mealListWidget.mealLabel = QLabel()
        tab.content.mealListWidget.mealLabel.setText("Meals: ")
        tab.content.mealListWidget.layout.addWidget(tab.content.mealListWidget.mealLabel)

        tab.content.mealListWidget.mealList = QListWidget(self)
        #fill list here
        tab.content.mealListWidget.meals = self.controller.userLoggedIn.meals
        for i in range(len(tab.content.mealListWidget.meals)):
            item = QtWidgets.QListWidgetItem()
            item.setText(str(tab.content.mealListWidget.meals[i]))
            tab.content.mealListWidget.mealList.addItem(item)

        tab.content.mealListWidget.mealList.itemActivated.connect(self.mealItemActivated_event)
        tab.content.mealListWidget.layout.addWidget(tab.content.mealListWidget.mealList)

        # Vertically aligned list of exercises with label
        tab.content.exerciseListWidget = QWidget()
        tab.content.layout.addWidget(tab.content.exerciseListWidget)
        tab.content.exerciseListWidget.layout = QVBoxLayout(tab.content.exerciseListWidget)
        tab.content.exerciseListWidget.setLayout(tab.content.exerciseListWidget.layout)

        tab.content.exerciseListWidget.exerciseLabel = QLabel()
        tab.content.exerciseListWidget.exerciseLabel.setText("Exercises: ")
        tab.content.exerciseListWidget.layout.addWidget(tab.content.exerciseListWidget.exerciseLabel)

        tab.content.exerciseListWidget.exerciseList = QListWidget(self)
        # fill list here
        tab.content.exerciseListWidget.exercises = self.controller.getUserExercises()
        for i in range(len(tab.content.exerciseListWidget.exercises)):
            item = QtWidgets.QListWidgetItem()
            item.setText(str(tab.content.exerciseListWidget.exercises[i]))
            tab.content.exerciseListWidget.exerciseList.addItem(item)
        tab.content.exerciseListWidget.exerciseList.itemActivated.connect(self.exerciseItemActivated_event)
        tab.content.exerciseListWidget.layout.addWidget(tab.content.exerciseListWidget.exerciseList)

        #Create vertical layout box within horizontal layout box
        tab.content.vBox = QWidget()
        self.mainTab = tab.content.vBox
        tab.content.layout.addWidget(tab.content.vBox)
        tab.content.vBox.layout = QVBoxLayout(tab.content.vBox)
        tab.content.vBox.setLayout(tab.content.vBox.layout)

        tab.content.vBox.mealButton = QPushButton('Add meal')
        tab.content.vBox.mealButton.clicked.connect(self.addMeal)
        tab.content.vBox.layout.addWidget(tab.content.vBox.mealButton)
        tab.content.vBox.exerciseButton = QPushButton('Add exercise')
        tab.content.vBox.exerciseButton.clicked.connect(self.addExercise)
        tab.content.vBox.layout.addWidget(tab.content.vBox.exerciseButton)
        tab.content.vBox.weightButton = QPushButton('Record weight')
        tab.content.vBox.weightButton.clicked.connect(self.recordWeight)
        tab.content.vBox.layout.addWidget(tab.content.vBox.weightButton)
        tab.content.vBox.logoutButton = QPushButton('logout')
        tab.content.vBox.logoutButton.clicked.connect(self.logout)
        tab.content.vBox.layout.addWidget(tab.content.vBox.logoutButton)

        tab.content.vBox.infoLabel = QLabel()
        tab.content.vBox.infoLabel.setText("Info: ")
        tab.content.vBox.layout.addWidget(tab.content.vBox.infoLabel)

        tab.content.vBox.textBox = QtWidgets.QPlainTextEdit()
        self.mainTextBox = tab.content.vBox.textBox
        self.loadMainInfoBox(tab.content.vBox, tab.content.vBox.textBox)

        return tab

    def shareGroup(self):
        print("share group")

    def leaveGroup(self):
        print("leave group")

    def groupItemActivated_event(self, item):
        self.groupText = self.controller.getGroup(item.text())
        self.groupInfo = ("Group Name: " + str(self.groupText[0][1]))
        self.loadGroupInfoBox(self.groupTab, self.groupTextBox)

    def loadGroupInfoBox(self, tab, textBox):
        textBox.setPlainText(str(self.groupInfo))
        textBox.setReadOnly(True)
        tab.layout.addWidget(textBox)

    def createGroupsTab(self):
        tab = QWidget()
        tab.layout = QHBoxLayout(self)
        tab.setLayout(tab.layout)

        #Vertically aligned list of groups with label
        tab.groupListWidget = QWidget()
        tab.groupListWidget.layout = QVBoxLayout(tab.groupListWidget)
        tab.layout.addWidget(tab.groupListWidget)
        tab.groupListWidget.groupListLabel = QLabel()
        tab.groupListWidget.groupListLabel.setText("Groups: ")
        tab.groupListWidget.layout.addWidget(tab.groupListWidget.groupListLabel)

        tab.groupListWidget.groupList = QListWidget(self)
        #fill list here
        tab.groupListWidget.groups = self.controller.getUserGroups()
        for i in range(len(tab.groupListWidget.groups)):
            item = QtWidgets.QListWidgetItem()
            item.setText(str(tab.groupListWidget.groups[i][1]))
            tab.groupListWidget.groupList.addItem(item)
        tab.groupListWidget.groupList.itemActivated.connect(self.groupItemActivated_event)
        tab.groupListWidget.layout.addWidget(tab.groupListWidget.groupList)
        #Vertically aligned buttons for sharing and leaving groups
        tab.selectGroupWidget = QWidget()
        self.groupTab = tab.selectGroupWidget
        tab.selectGroupWidget.layout = QVBoxLayout(tab.selectGroupWidget)
        tab.layout.addWidget(tab.selectGroupWidget)

        #selected group label
        tab.selectGroupWidget.selectGroupLabel = QLabel()
        tab.selectGroupWidget.selectGroupLabel.setText("Selected Group: ")
        tab.selectGroupWidget.layout.addWidget(tab.selectGroupWidget.selectGroupLabel)

        #share and leave group buttons
        tab.selectGroupWidget.shareGroupButton = QPushButton("Share")
        tab.selectGroupWidget.shareGroupButton.clicked.connect(self.shareGroup)
        tab.selectGroupWidget.layout.addWidget(tab.selectGroupWidget.shareGroupButton)
        tab.selectGroupWidget.leaveGroupButton = QPushButton("Leave")
        tab.selectGroupWidget.leaveGroupButton.clicked.connect(self.leaveGroup)
        tab.selectGroupWidget.layout.addWidget(tab.selectGroupWidget.leaveGroupButton)

        #selected group info box
        tab.selectGroupWidget.textBox = QtWidgets.QPlainTextEdit()
        self.groupTextBox = tab.selectGroupWidget.textBox
        self.loadGroupInfoBox(self.groupTab, tab.selectGroupWidget.textBox)


        return tab

    def goalItemActivated_event(self, item):
        index = self.goalsTab.goalWidget.goalList.currentRow()
        if index == -1:
            index = self.goalsTab.finishedGoalWidget.finishedGoalList.currentRow()
        self.goalText = self.controller.userLoggedIn.goals[index]
        self.goalInfo = ("Goal Name: " + str(self.goalText) + "\n\n" +
                         "Date Added: " + str(self.goalText.startDate) + "\n\n" +
                         "Target Date: " + str(self.goalText.targetDate) + "\n\n")
        if self.goalText.duration is not None:
            self.goalInfo += "Target duration: " + str(self.goalText.durationExercised) + " exercised out of required " + str(self.goalText.duration) + "\n\n"
        if self.goalText.distance is not None:
            self.goalInfo += "Target distance: " + str(self.goalText.distanceMoved) + "m moved out of required " + str(self.goalText.distance) + "m\n\n"

        self.loadGoalInfoBox(self.goalTab, self.goalTextBox)

    def loadGoalInfoBox(self, tab, textBox):
        textBox.setPlainText(str(self.goalInfo))
        textBox.setReadOnly(True)
        tab.layout.addWidget(textBox)

    def createGoalsTab(self):
        tab = QWidget()
        tab.layout = QHBoxLayout(self)
        tab.setLayout(tab.layout)

        #Vertically aligned list of goals
        tab.goalWidget = QWidget()
        tab.goalWidget.layout = QVBoxLayout(tab.goalWidget)
        tab.layout.addWidget(tab.goalWidget)
        tab.goalWidget.goalLabel = QLabel()
        tab.goalWidget.goalLabel.setText("Ongoing Goals: ")
        tab.goalWidget.layout.addWidget(tab.goalWidget.goalLabel)
        tab.goalWidget.goalList = QListWidget(self)
        #fill list here
        tab.goalWidget.goals = self.controller.getUserGoals()
        tab.goalWidget.ongoingGoals = []
        for i in range(len(tab.goalWidget.goals)):
            if tab.goalWidget.goals[i].finished == False and tab.goalWidget.goals[i].isMet == False:
                tab.goalWidget.ongoingGoals.append(tab.goalWidget.goals[i])
        for i in range(len(tab.goalWidget.ongoingGoals)):
            item = QtWidgets.QListWidgetItem()
            item.setText(str(tab.goalWidget.ongoingGoals[i]))
            tab.goalWidget.goalList.addItem(item)
        tab.goalWidget.goalList.itemActivated.connect(self.goalItemActivated_event)
        tab.goalWidget.layout.addWidget(tab.goalWidget.goalList)

        #Vertically aligned list of completed Goals
        tab.finishedGoalWidget = QWidget()
        tab.finishedGoalWidget.layout = QVBoxLayout(tab.finishedGoalWidget)
        tab.layout.addWidget(tab.finishedGoalWidget)
        tab.finishedGoalWidget.finishedGoalLabel = QLabel()
        tab.finishedGoalWidget.finishedGoalLabel.setText("Finished Goals: ")
        tab.finishedGoalWidget.layout.addWidget(tab.finishedGoalWidget.finishedGoalLabel)
        tab.finishedGoalWidget.finishedGoalList = QListWidget(self)
        #fill list here
        tab.finishedGoalWidget.goals = self.controller.getUserGoals()
        tab.finishedGoalWidget.finishedGoals = []
        for i in range(len(tab.finishedGoalWidget.goals)):
            if tab.finishedGoalWidget.goals[i].finished == 'TRUE' or tab.finishedGoalWidget.goals[i].isMet == 'TRUE':
                tab.finishedGoalWidget.finishedGoals.append(tab.finishedGoalWidget.goals[i])

        for i in range(len(tab.finishedGoalWidget.finishedGoals)):
            item = QtWidgets.QListWidgetItem()
            item.setText(str(tab.finishedGoalWidget.finishedGoals[i]))
            tab.finishedGoalWidget.finishedGoalList.addItem(item)
        tab.finishedGoalWidget.finishedGoalList.itemActivated.connect(self.goalItemActivated_event)
        tab.finishedGoalWidget.layout.addWidget(tab.finishedGoalWidget.finishedGoalList)

        #Column for info about goals and new goal button
        tab.infoWidget = QWidget()
        self.goalTab = tab.infoWidget
        tab.infoWidget.layout = QVBoxLayout(tab.infoWidget)
        tab.layout.addWidget(tab.infoWidget)

        #Button to add a new goal
        tab.infoWidget.newGoalButton = QPushButton("New Goal")
        tab.infoWidget.newGoalButton.clicked.connect(self.addGoal)
        tab.infoWidget.layout.addWidget(tab.infoWidget.newGoalButton)

        #goal info label
        tab.infoWidget.infoLabel = QLabel()
        tab.infoWidget.infoLabel.setText("Goal Info: ")
        tab.infoWidget.layout.addWidget(tab.infoWidget.infoLabel)

        #goal info text box
        tab.infoWidget.textBox = QtWidgets.QPlainTextEdit()
        self.goalTextBox = tab.infoWidget.textBox
        self.loadGoalInfoBox(tab.infoWidget, tab.infoWidget.textBox)

        return tab
