from src.App import *
from src.model.User import User
from src.model.Goal import ExerciseGoal, Goal, WeightGoal
import datetime

class NewGoal(QWidget):
    def __init__(self, parent, controller):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.layout = QHBoxLayout()
        self.setLayout(self.layout)

        self.goalFields = self.goalEntries()
        self.layout.addWidget(self.goalFields)
        self.exerciseFields = self.exerciseGoalEntries()
        self.layout.addWidget(self.exerciseFields)
        self.weightFields = self.weightGoalEntries()
        self.layout.addWidget(self.weightFields)

        self.buttonColumn = QWidget()
        self.buttonColumn.layout = QVBoxLayout(self.buttonColumn)
        self.layout.addWidget(self.buttonColumn)

        self.buttonColumn.createGoalButton = QPushButton("Create Goal")
        self.buttonColumn.createGoalButton.clicked.connect(self.submitGoal)
        self.buttonColumn.layout.addWidget(self.buttonColumn.createGoalButton)

        self.buttonColumn.cancelButton = QPushButton("Cancel")
        self.buttonColumn.cancelButton.clicked.connect(self.cancel)
        self.buttonColumn.layout.addWidget(self.buttonColumn.cancelButton)

        #Error message for invalid inputs
        self.errorMessage = QLabel()
        self.buttonColumn.layout.addWidget(self.errorMessage)

        self.exerciseType = 0
        self.currentExercise = self.controller.userLoggedIn.getExerciseTypes()[0]

    def submitGoal(self):
        #Check title entry
        if self.goalFields.goalNameInput.text() == "":
            self.errorMessage.setText("<font color='red'>Please enter a title</font>")
            return

        #Check duration entry
        if self.currentExercise.hasDuration:
            if not self.exerciseFields.durationBox.hours.text().isdigit():
                self.errorMessage.setText("<font color='red'>Hours must be a positive whole number</font>")
                return
            if not self.exerciseFields.durationBox.minutes.text().isdigit():
                self.errorMessage.setText("<font color='red'>Minutes must be a positive whole number</font>")
                return
            elif (int)(self.exerciseFields.durationBox.minutes.text()) > 60:
                self.errorMessage.setText("<font color='red'>Minutes cant be more than 60</font>")
                return
            if not self.exerciseFields.durationBox.seconds.text().isdigit():
                self.errorMessage.setText("<font color='red'>Seconds must be a positive whole number</font>")
                return
            elif (int)(self.exerciseFields.durationBox.seconds.text()) > 60:
                self.errorMessage.setText("<font color='red'>Seconds cant be more than 60</font>")
                return

        #Check distance entry
        if self.currentExercise.hasDistance:
            if not self.exerciseFields.distanceAmount.text().isdigit():
                self.errorMessage.setText("<font color='red'>Distance must be a positive whole number</font>")
                return

        title = self.goalFields.goalNameInput.text()
        startDate = datetime.datetime.today()
        endDate = self.goalFields.goalFinishInput.date().toPyDate()
        exerciseType = self.currentExercise.name
        duration = None
        distance = None
        if self.currentExercise.hasDuration:
            duration = datetime.time(int(self.exerciseFields.durationBox.hours.text()), int(self.exerciseFields.durationBox.minutes.text()), int(self.exerciseFields.durationBox.seconds.text()))
        if self.currentExercise.hasDistance:
            distance = int(self.exerciseFields.distanceAmount.text())

        goal = ExerciseGoal(startDate, endDate, title, False, False, exerciseType, duration, distance)
        self.controller.createNewExerciseGoal(goal)
        self.parent.confirmOnPopupClose("Goal created")

    def selectType(self, i):
        self.exerciseType = i
        self.currentExercise = self.controller.userLoggedIn.getExerciseTypes()[i]

        if self.currentExercise.hasDuration:
            self.exerciseFields.durationLabel.show()
            self.exerciseFields.durationBox.show()
        else:
            self.exerciseFields.durationLabel.hide()
            self.exerciseFields.durationBox.hide()

        if self.currentExercise.hasDistance:
            self.exerciseFields.distanceAmountLabel.show()
            self.exerciseFields.distanceAmount.show()
        else:
            self.exerciseFields.distanceAmountLabel.hide()
            self.exerciseFields.distanceAmount.hide()

    def cancel(self):
        self.parent.closePopup()

    def exerciseGoalEntries(self):
        dataColumn = QWidget()
        dataColumn.layout = QVBoxLayout(dataColumn)

        dataColumn.exerciseDropLabel = QLabel()
        dataColumn.exerciseDropLabel.setText("Exercise Type: ")
        dataColumn.layout.addWidget(dataColumn.exerciseDropLabel)
        dataColumn.exerciseDropBox = QComboBox()
        dataColumn.exerciseDropBox.addItems(map(str, User.DEFAULT_USER_EXERCISES))
        dataColumn.exerciseDropBox.currentIndexChanged.connect(self.selectType)
        dataColumn.layout.addWidget(dataColumn.exerciseDropBox)
        exerciseType = 0

        dataColumn.durationLabel = QLabel()
        dataColumn.durationLabel.setText("Duration: ")
        dataColumn.layout.addWidget(dataColumn.durationLabel)

        dataColumn.durationBox = QWidget()
        dataColumn.durationBox.layout = QHBoxLayout()
        dataColumn.durationBox.setLayout(dataColumn.durationBox.layout)
        dataColumn.layout.addWidget(dataColumn.durationBox)

        dataColumn.durationBox.hours = QLineEdit(dataColumn)
        dataColumn.durationBox.hours.setPlaceholderText("HH")
        dataColumn.durationBox.hours.setMaxLength(2)
        dataColumn.durationBox.hours.setMaximumWidth(22)
        dataColumn.durationBox.layout.addWidget(dataColumn.durationBox.hours)
        dataColumn.durationBox.layout.addWidget(QLabel(" :"))
        dataColumn.durationBox.minutes = QLineEdit(dataColumn)
        dataColumn.durationBox.minutes.setPlaceholderText("MM")
        dataColumn.durationBox.minutes.setMaxLength(2)
        dataColumn.durationBox.minutes.setMaximumWidth(22)
        dataColumn.durationBox.layout.addWidget(dataColumn.durationBox.minutes)
        dataColumn.durationBox.layout.addWidget(QLabel(" :"))
        dataColumn.durationBox.seconds = QLineEdit(dataColumn)
        dataColumn.durationBox.seconds.setPlaceholderText("SS")
        dataColumn.durationBox.seconds.setMaxLength(2)
        dataColumn.durationBox.seconds.setMaximumWidth(22)
        dataColumn.durationBox.layout.addWidget(dataColumn.durationBox.seconds)

        dataColumn.distanceAmountLabel = QLabel()
        dataColumn.distanceAmountLabel.setText("Distance")
        dataColumn.layout.addWidget(dataColumn.distanceAmountLabel)
        dataColumn.distanceAmount = QLineEdit(dataColumn)
        dataColumn.distanceAmount.setPlaceholderText("(M)")
        dataColumn.layout.addWidget(dataColumn.distanceAmount)

        return dataColumn

    def weightGoalEntries(self):
        dataColumn = QWidget()
        dataColumn.layout = QVBoxLayout(dataColumn)

        return dataColumn

    def goalEntries(self):
        dataColumn = QWidget()
        dataColumn.layout = QVBoxLayout(dataColumn)

        dataColumn.goalNameLabel = QLabel()
        dataColumn.goalNameLabel.setText("Goal Name: ")
        dataColumn.layout.addWidget(dataColumn.goalNameLabel)
        dataColumn.goalNameInput = QLineEdit()
        dataColumn.goalNameInput.setPlaceholderText("e.g Run 30 Kilometers")
        dataColumn.layout.addWidget(dataColumn.goalNameInput)

        dataColumn.goalFinishLabel = QLabel()
        dataColumn.goalFinishLabel.setText("Finish Date: ")
        dataColumn.layout.addWidget(dataColumn.goalFinishLabel)
        dataColumn.goalFinishInput = QDateEdit()
        dataColumn.goalFinishInput.setMinimumDate(QDate.currentDate().addDays(1))
        dataColumn.layout.addWidget(dataColumn.goalFinishInput)

        return dataColumn

    #Define goal type buttons
    def goalTypeButtons(self):
        main = QWidget()
        main.layout = QVBoxLayout()
        main.setLayout(main.layout)

        main.buttons = QWidget()
        main.buttons.layout = QHBoxLayout()
        main.buttons.setLayout(main.buttons.layout)

        main.buttons.exerciseButton = QRadioButton("Exercise goal")
        main.buttons.exerciseButton.toggled.connect(self.toggleButtons)
        main.buttons.layout.addWidget(main.buttons.exerciseButton)

        main.buttons.weightButton = QRadioButton("Weight goal")
        main.buttons.weightButton.toggled.connect(self.toggleButtons)
        main.buttons.layout.addWidget(main.buttons.weightButton)

        return main

    def toggleButtons(self, button):
        return

