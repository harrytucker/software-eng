from src.App import *
from src.model.User import User
import re

class NewUserScreen(QWidget):
    def __init__(self, parent, controller):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.dbController = DatabaseController()
        self.makeUserFlag = True
        self.errorID = 0
        self.emailPattern = re.compile("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
        self.layout = QVBoxLayout()

        self.logoLabel = QLabel()
        self.logoLabel.pixmap = QPixmap("uiFiles/logo.png")
        self.logoLabel.pixmap = self.logoLabel.pixmap.scaled(600, 600, Qt.KeepAspectRatio)
        self.logoLabel.setPixmap(self.logoLabel.pixmap)
        self.layout.addWidget(self.logoLabel)

        self.usernameLabel = QLabel()
        self.usernameLabel.setText("Username:")
        self.layout.addWidget(self.usernameLabel)
        self.username = QLineEdit(self)
        self.username.setPlaceholderText("e.g. john123")
        self.layout.addWidget(self.username)

        self.fullNameLabel = QLabel()
        self.fullNameLabel.setText("Full Name:")
        self.layout.addWidget(self.fullNameLabel)
        self.fullName = QLineEdit(self)
        self.fullName.setPlaceholderText("e.g. John Smith")
        self.layout.addWidget(self.fullName)

        self.heightLabel = QLabel()
        self.heightLabel.setText("Height:")
        self.layout.addWidget(self.heightLabel)
        self.height = QLineEdit(self)
        self.height.setPlaceholderText("Metres")
        self.layout.addWidget(self.height)

        self.weightLabel = QLabel()
        self.weightLabel.setText("Weight:")
        self.layout.addWidget(self.weightLabel)
        self.weight = QLineEdit(self)
        self.weight.setPlaceholderText("Kg")
        self.layout.addWidget(self.weight)

        self.emailLabel = QLabel()
        self.emailLabel.setText("Email")
        self.layout.addWidget(self.emailLabel)
        self.email = QLineEdit(self)
        self.email.setPlaceholderText("example@example.com")
        self.layout.addWidget(self.email)

        self.passwordLabel = QLabel()
        self.passwordLabel.setText("Password:")
        self.layout.addWidget(self.passwordLabel)
        self.password = QLineEdit(self)
        self.password.setPlaceholderText("Min 8 characters")
        self.password.setEchoMode(QLineEdit.Password)
        self.layout.addWidget(self.password)

        self.invalidValueLabelPlaceholder = QVBoxLayout()
        self.layout.addLayout(self.invalidValueLabelPlaceholder)
        self.invalidEntryMade = False

        self.invalidEntryLabel = QLabel()
        self.submitButton = QPushButton('Submit')
        self.submitButton.clicked.connect(self.submit)
        self.layout.addWidget(self.submitButton)

        self.newUserButton = QPushButton('Cancel')
        self.newUserButton.clicked.connect(self.cancel)
        self.layout.addWidget(self.newUserButton)

        self.setLayout(self.layout)

    def submit(self):
        if self.username.text() == "" or self.email.text() == "" or self.fullName.text() == "":
            self.errorID = 0
            self.invalidEntry()
        elif len(self.password.text()) < 8:
            self.errorID = 1
            self.invalidEntry()
        elif not self.emailPattern.match(self.email.text()):
            self.errorID = 2
            self.invalidEntry()
        elif self.dbController.checkForExistingUser(self.username.text()) == -1:
            self.errorID = 3
            self.invalidEntry()
        else:
            self.makeUserFlag = True

        try:
            if not (float(self.height.text()) > 0 and float(self.weight.text()) > 0):
                self.errorID = 4
                self.invalidEntry()
        except ValueError:
            self.errorID = 5
            self.invalidEntry()

        if self.makeUserFlag == True:
            newUser = User(self.username.text(),
                           self.email.text(),
                           self.fullName.text(),
                           float(self.weight.text()),
                           float(self.height.text()),
                           self.password.text())
            self.controller.addNewUser(newUser)

            self.parent.setCurrentScreen("login")

    def cancel(self):
        self.parent.setCurrentScreen("login")

    def invalidEntry(self):
        self.makeUserFlag = False
        if self.errorID == 0:
            self.invalidEntryLabel.setText("<font color='red'>Some entries are empty, and must be filled</font>")
        elif self.errorID == 1:
            self.invalidEntryLabel.setText("<font color='red'>Password is too short</font>")
        elif self.errorID == 2:
            self.invalidEntryLabel.setText("<font color='red'>Invalid email address</font>")
        elif self.errorID == 3:
            self.invalidEntryLabel.setText("<font color='red'>Username already exists</font>")
        elif self.errorID == 4:
            self.invalidEntryLabel.setText("<font color='red'>Weight and Height entries must be positive non zero numbers</font>")
        elif self.errorID == 5:
            self.invalidEntryLabel.setText("<font color='red'>Weight and Height entries must be numbers</font>")

        if not self.invalidEntryMade:
            self.invalidEntryMade = True
            self.invalidValueLabelPlaceholder.addWidget(self.invalidEntryLabel)
