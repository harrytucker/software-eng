from src.App import *
from src.DatabaseController import DatabaseController


#Screen on popup window to record user weight
class NewWeight(QWidget):
    def __init__(self, parent, controller):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.dbController = DatabaseController()
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        #Current weight label
        self.currentWeightLabel = QLabel()
        self.currentWeightLabel.setText("Current weight: " + str(self.controller.userLoggedIn.weight) + "Kg                            ")
        self.layout.addWidget(self.currentWeightLabel)

        #New weight label and text box
        self.newWeightLabel = QLabel()
        self.newWeightLabel.setText("New weight: ")
        self.layout.addWidget(self.newWeightLabel)
        self.newWeight = QLineEdit(self)
        self.layout.addWidget(self.newWeight)

        #Add weight button
        self.addButton = QPushButton("Record weight")
        self.addButton.clicked.connect(self.submitNewWeight)
        self.layout.addWidget(self.addButton)

        #Cancel button
        self.cancelButton = QPushButton("Cancel")
        self.cancelButton.clicked.connect(self.cancel)
        self.layout.addWidget(self.cancelButton)

        #Error message for invalid inputs
        self.errorMessage = QLabel()
        self.layout.addWidget(self.errorMessage)

    #Check user inputs to see if they are valid before sending request to record new weight and then closing the popup
    def submitNewWeight(self):
        #Check weight entry
        if self.newWeight.text() == "":
            self.errorMessage.setText("<font color='red'>Please enter a new weight</font>")
            return

        try:
            if not (float(self.newWeight.text()) > 0 and float(self.newWeight.text()) > 0):
                self.errorMessage.setText("<font color='red'>New weight must be more than 0</font>")
                return
        except ValueError:
            self.errorMessage.setText("<font color='red'>New weight must be numerical</font>")
            return

        # update the user's weight
        self.dbController.updateUserWeight(self.parent.controller.userLoggedIn.username, float(self.newWeight.text()))

        #Pass new weight to controller and then close the popup
        self.parent.controller.recordWeight(float(self.newWeight.text()))
        self.parent.confirmOnPopupClose("weightRecorded")

    #Close the popup
    def cancel(self):
        self.parent.closePopup()