from src.App import *
from src.model.FoodAndDrink import MealType, Meal
import datetime

class NewMeal(QWidget):
    def __init__(self, parent, controller):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        #Title label
        self.title = QLabel()
        self.title.setText("Create new meal")
        self.layout.addWidget(self.title)

        #Horizontal layout box for checkBoxes and meal type drop down
        self.checkBoxes = QWidget()
        self.checkBoxes.layout = QHBoxLayout()
        self.checkBoxes.setLayout(self.checkBoxes.layout)
        self.layout.addWidget(self.checkBoxes)

        self.checkBoxes.foodCheck = QCheckBox("Food")
        self.checkBoxes.foodCheck.toggled.connect(self.toggleFoodCheck)
        self.checkBoxes.layout.addWidget(self.checkBoxes.foodCheck)

        self.checkBoxes.drinkCheck = QCheckBox("Drink")
        self.checkBoxes.drinkCheck.toggled.connect(self.toggleDrinkCheck)
        self.checkBoxes.layout.addWidget(self.checkBoxes.drinkCheck)

        #Vertical layout box for meal type label and drop down
        self.checkBoxes.typeBox = QWidget()
        self.checkBoxes.typeBox.layout = QVBoxLayout()
        self.checkBoxes.typeBox.setLayout(self.checkBoxes.typeBox.layout)
        self.checkBoxes.layout.addWidget(self.checkBoxes.typeBox)

        #Meal type label and drop down menu
        self.checkBoxes.typeBox.typeLabel = QLabel()
        self.checkBoxes.typeBox.typeLabel.setText("Meal type")
        self.checkBoxes.typeBox.layout.addWidget(self.checkBoxes.typeBox.typeLabel)

        self.checkBoxes.typeBox.type = QComboBox()
        self.checkBoxes.typeBox.type.addItems([m.name for m in MealType])
        self.checkBoxes.typeBox.type.currentIndexChanged.connect(self.selectMealType)
        self.checkBoxes.typeBox.layout.addWidget(self.checkBoxes.typeBox.type)
        self.currentMeal = 0

        #Horizontal layout box for food and drink details and confirm or cancel buttons
        self.hBox = QWidget()
        self.hBox.layout = QHBoxLayout()
        self.hBox.setLayout(self.hBox.layout)
        self.layout.addWidget(self.hBox)

        #Vertical layout box within horizontal box for food details
        self.hBox.foodDetails = QWidget()
        self.hBox.foodDetails.layout = QVBoxLayout()
        self.hBox.foodDetails.setLayout(self.hBox.foodDetails.layout)
        self.hBox.layout.addWidget(self.hBox.foodDetails)

        #Food type label and drop down menu
        self.hBox.foodDetails.typeLabel = QLabel()
        self.hBox.foodDetails.typeLabel.setText("Food type")
        self.hBox.foodDetails.layout.addWidget(self.hBox.foodDetails.typeLabel)

        self.hBox.foodDetails.type = QComboBox()
        self.hBox.foodDetails.type.addItems(map(str, self.controller.userLoggedIn.foods))
        self.hBox.foodDetails.type.currentIndexChanged.connect(self.selectFoodType)
        self.hBox.foodDetails.layout.addWidget(self.hBox.foodDetails.type)
        self.foodType = 0
        self.currentFood = None

        #Food amount label and text box
        self.hBox.foodDetails.amountLabel = QLabel()
        self.hBox.foodDetails.amountLabel.setText("Amount")
        self.hBox.foodDetails.layout.addWidget(self.hBox.foodDetails.amountLabel)
        self.hBox.foodDetails.amount = QLineEdit(self.hBox.foodDetails)
        self.hBox.foodDetails.amount.setPlaceholderText("g")
        self.hBox.foodDetails.layout.addWidget(self.hBox.foodDetails.amount)

        #Vertical layout box within horizontal box for drink details
        self.hBox.drinkDetails = QWidget()
        self.hBox.drinkDetails.layout = QVBoxLayout()
        self.hBox.drinkDetails.setLayout(self.hBox.drinkDetails.layout)
        self.hBox.layout.addWidget(self.hBox.drinkDetails)

        #Drink type label and drop down menu
        self.hBox.drinkDetails.typeLabel = QLabel()
        self.hBox.drinkDetails.typeLabel.setText("Drink type")
        self.hBox.drinkDetails.layout.addWidget(self.hBox.drinkDetails.typeLabel)

        self.hBox.drinkDetails.type = QComboBox()
        self.hBox.drinkDetails.type.addItems(map(str, self.controller.userLoggedIn.drinks))
        self.hBox.drinkDetails.type.currentIndexChanged.connect(self.selectDrinkType)
        self.hBox.drinkDetails.layout.addWidget(self.hBox.drinkDetails.type)
        self.drinkType = 0
        self.currentDrink = None

        #Drink amount label and text box
        self.hBox.drinkDetails.amountLabel = QLabel()
        self.hBox.drinkDetails.amountLabel.setText("Amount")
        self.hBox.drinkDetails.layout.addWidget(self.hBox.drinkDetails.amountLabel)
        self.hBox.drinkDetails.amount = QLineEdit(self.hBox.drinkDetails)
        self.hBox.drinkDetails.amount.setPlaceholderText("ml")
        self.hBox.drinkDetails.layout.addWidget(self.hBox.drinkDetails.amount)

        #Vertical box to hold buttons to add meal, food, drink and cancel
        self.hBox.buttonsBox = QWidget()
        self.hBox.layout.addWidget(self.hBox.buttonsBox)
        self.hBox.buttonsBox.layout = QVBoxLayout()
        self.hBox.buttonsBox.setLayout(self.hBox.buttonsBox.layout)

        self.hBox.buttonsBox.addMealButton = QPushButton('Add Meal')
        self.hBox.buttonsBox.addMealButton.clicked.connect(self.addMeal)
        self.hBox.buttonsBox.layout.addWidget(self.hBox.buttonsBox.addMealButton)

        self.hBox.buttonsBox.addFoodButton = QPushButton('Add new food type')
        self.hBox.buttonsBox.addFoodButton.clicked.connect(self.addNewFood)
        self.hBox.buttonsBox.layout.addWidget(self.hBox.buttonsBox.addFoodButton)

        self.hBox.buttonsBox.addDrinkButton = QPushButton('Add new drink type')
        self.hBox.buttonsBox.addDrinkButton.clicked.connect(self.addNewDrink)
        self.hBox.buttonsBox.layout.addWidget(self.hBox.buttonsBox.addDrinkButton)

        self.hBox.buttonsBox.cancelButton = QPushButton('Cancel')
        self.hBox.buttonsBox.cancelButton.clicked.connect(self.cancel)
        self.hBox.buttonsBox.layout.addWidget(self.hBox.buttonsBox.cancelButton)

        #Error message for invalid inputs
        self.errorMessage = QLabel()
        self.layout.addWidget(self.errorMessage)

        #Initialise type values for food, drink and checkBoxes
        self.checkBoxes.foodCheck.setChecked(True)
        self.checkBoxes.drinkCheck.setChecked(False)
        self.toggleDrinkCheck(False)
        self.selectFoodType(0)
        self.selectDrinkType(0)

    #Check if inputs are valid before adding a new meal to user and close popup window
    def addMeal(self):
        #Invalid entry checks
        if self.checkBoxes.foodCheck.isChecked() and not self.hBox.foodDetails.amount.text().isdigit():
            self.errorMessage.setText("<font color='red'>Food amount must be a positive whole number</font>")
            return
        if self.checkBoxes.drinkCheck.isChecked() and not self.hBox.drinkDetails.amount.text().isdigit():
            self.errorMessage.setText("<font color='red'>Drink amount must be a positive whole number</font>")
            return
        #Get new meal parameters
        dateAdded = datetime.date.today()
        now = datetime.datetime.now()
        timeAdded = datetime.time(now.hour, now.minute, now.second)
        mealOfTheDay = self.currentMeal
        food = None
        foodAmount = 0
        drink = None
        drinkAmount = 0
        if self.checkBoxes.foodCheck.isChecked():
            food = self.currentFood
            foodAmount = int(self.hBox.foodDetails.amount.text())
        if self.checkBoxes.drinkCheck.isChecked():
            drink = self.currentDrink
            drinkAmount = int(self.hBox.drinkDetails.amount.text())

        self.controller.addNewMeal(dateAdded, timeAdded, mealOfTheDay, food, foodAmount, drink, drinkAmount)
        self.parent.confirmOnPopupClose("Meal added")

    #Change popup window screen to create new food screen
    def addNewFood(self):
        self.parent.changePopupToAddFood()

    #Change popup window screen to create new drink screen
    def addNewDrink(self):
        self.parent.changePopupToAddDrink()

    #Close popup window
    def cancel(self):
        self.parent.closePopup()

    #invoked when user checks or unchecks food check box
    def toggleFoodCheck(self, checked):
        if checked:
            self.hBox.foodDetails.show()
        else:
            if not self.checkBoxes.drinkCheck.isChecked():
                self.checkBoxes.drinkCheck.setChecked(True)
            self.hBox.foodDetails.hide()

    #invoked when user checks or unchecks drink check box
    def toggleDrinkCheck(self, checked):
        if checked:
            self.hBox.drinkDetails.show()
        else:
            if not self.checkBoxes.foodCheck.isChecked():
                self.checkBoxes.foodCheck.setChecked(True)
            self.hBox.drinkDetails.hide()

    #invoked when user chooses a type of food from drop down
    def selectFoodType(self, i):
        self.foodType = i
        self.currentFood = self.controller.userLoggedIn.foods[i]

    #invoked when user chooses a type of drink from drop down
    def selectDrinkType(self, i):
        self.drinkType = i
        self.currentDrink = self.controller.userLoggedIn.drinks[i]

    #invoked when user chooses a type of meal from drop down
    def selectMealType(self, i):
        self.currentMeal = i

#Screen on popup window to add a new food
class AddNewFood(QWidget):
    def __init__(self, parent, controller):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        #Title label
        self.title = QLabel()
        self.title.setText("Create new food")
        self.layout.addWidget(self.title)

        #Horizontal layout box
        self.hBox = QWidget()
        self.hBox.layout = QHBoxLayout()
        self.hBox.setLayout(self.hBox.layout)
        self.layout.addWidget(self.hBox)

        #Vertical layout box within horizontal box for food details
        self.hBox.foodDetails = QWidget()
        self.hBox.foodDetails.layout = QVBoxLayout()
        self.hBox.foodDetails.setLayout(self.hBox.foodDetails.layout)
        self.hBox.layout.addWidget(self.hBox.foodDetails)

        #Name label and text box
        self.hBox.foodDetails.foodLabel = QLabel()
        self.hBox.foodDetails.foodLabel.setText("Food name")
        self.hBox.foodDetails.layout.addWidget(self.hBox.foodDetails.foodLabel)
        self.hBox.foodDetails.foodName = QLineEdit(self.hBox.foodDetails)
        self.hBox.foodDetails.layout.addWidget(self.hBox.foodDetails.foodName)

        #Calories label and text box
        self.hBox.foodDetails.foodAmountLabel = QLabel()
        self.hBox.foodDetails.foodAmountLabel.setText("Calories per 100g")
        self.hBox.foodDetails.layout.addWidget(self.hBox.foodDetails.foodAmountLabel)
        self.hBox.foodDetails.foodAmount = QLineEdit(self.hBox.foodDetails)
        self.hBox.foodDetails.layout.addWidget(self.hBox.foodDetails.foodAmount)

        #Vertical layout box within horizontal box for confirm and cancel buttons
        self.hBox.sideButtons = QWidget()
        self.hBox.sideButtons.layout = QVBoxLayout()
        self.hBox.sideButtons.setLayout(self.hBox.sideButtons.layout)
        self.hBox.layout.addWidget(self.hBox.sideButtons)

        #Add food button
        self.hBox.sideButtons.addButton = QPushButton("Add food")
        self.hBox.sideButtons.addButton.clicked.connect(self.submitNewFood)
        self.hBox.sideButtons.layout.addWidget(self.hBox.sideButtons.addButton)

        #Cancel button
        self.hBox.sideButtons.cancelButton = QPushButton("Cancel")
        self.hBox.sideButtons.cancelButton.clicked.connect(self.changeToAddMealScreen)
        self.hBox.sideButtons.layout.addWidget(self.hBox.sideButtons.cancelButton)

        #Error message for invalid inputs
        self.errorMessage = QLabel()
        self.layout.addWidget(self.errorMessage)

    #Changes popup window screen to add meal screen
    def changeToAddMealScreen(self):
        self.parent.changePopupToAddMeal()

    #Check if user entries are valid and create new food
    def submitNewFood(self):
        if self.hBox.foodDetails.foodName.text() == "":
            self.errorMessage.setText("<font color='red'>Please enter a name</font>")
            return
        elif not self.hBox.foodDetails.foodAmount.text().isdigit():
            self.errorMessage.setText("<font color='red'>Amount entry must be a positive whole number</font>")
            return
        name = self.hBox.foodDetails.foodName.text()
        caloriesPer100g = (int)(self.hBox.foodDetails.foodAmount.text())
        self.controller.addNewFood(name, caloriesPer100g)
        self.changeToAddMealScreen()

#Screen on popup window to add a new drink
class AddNewDrink(QWidget):
    def __init__(self, parent, controller):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        #Title label
        self.title = QLabel()
        self.title.setText("Create new Drink")
        self.layout.addWidget(self.title)

        #Horizontal layout box
        self.hBox = QWidget()
        self.hBox.layout = QHBoxLayout()
        self.hBox.setLayout(self.hBox.layout)
        self.layout.addWidget(self.hBox)

        #Vertical layout box within horizontal box for drink details
        self.hBox.drinkDetails = QWidget()
        self.hBox.drinkDetails.layout = QVBoxLayout()
        self.hBox.drinkDetails.setLayout(self.hBox.drinkDetails.layout)
        self.hBox.layout.addWidget(self.hBox.drinkDetails)

        #Name label and text box
        self.hBox.drinkDetails.drinkLabel = QLabel()
        self.hBox.drinkDetails.drinkLabel.setText("Drink name")
        self.hBox.drinkDetails.layout.addWidget(self.hBox.drinkDetails.drinkLabel)
        self.hBox.drinkDetails.drinkName = QLineEdit(self.hBox.drinkDetails)
        self.hBox.drinkDetails.layout.addWidget(self.hBox.drinkDetails.drinkName)

        #Calories label and text box
        self.hBox.drinkDetails.drinkAmountLabel = QLabel()
        self.hBox.drinkDetails.drinkAmountLabel.setText("Calories per 100ml")
        self.hBox.drinkDetails.layout.addWidget(self.hBox.drinkDetails.drinkAmountLabel)
        self.hBox.drinkDetails.drinkAmount = QLineEdit(self.hBox.drinkDetails)
        self.hBox.drinkDetails.layout.addWidget(self.hBox.drinkDetails.drinkAmount)

        #Vertical layout box within horizontal box for confirm and cancel buttons
        self.hBox.sideButtons = QWidget()
        self.hBox.sideButtons.layout = QVBoxLayout()
        self.hBox.sideButtons.setLayout(self.hBox.sideButtons.layout)
        self.hBox.layout.addWidget(self.hBox.sideButtons)

        #Add drink button
        self.hBox.sideButtons.addButton = QPushButton("Create drink")
        self.hBox.sideButtons.addButton.clicked.connect(self.submitNewDrink)
        self.hBox.sideButtons.layout.addWidget(self.hBox.sideButtons.addButton)

        #Cancel button
        self.hBox.sideButtons.cancelButton = QPushButton("Cancel")
        self.hBox.sideButtons.cancelButton.clicked.connect(self.changeToAddMealScreen)
        self.hBox.sideButtons.layout.addWidget(self.hBox.sideButtons.cancelButton)

        #Error message for invalid inputs
        self.errorMessage = QLabel()
        self.layout.addWidget(self.errorMessage)

    #Changes popup window screen to add meal screen
    def changeToAddMealScreen(self):
        self.parent.changePopupToAddMeal()

    #Check if user entries are valid and create new drink
    def submitNewDrink(self):
        if self.hBox.drinkDetails.drinkName.text() == "":
            self.errorMessage.setText("<font color='red'>Please enter a name</font>")
            return
        elif not self.hBox.drinkDetails.drinkAmount.text().isdigit():
            self.errorMessage.setText("<font color='red'>Amount entry must be a positive whole number</font>")
            return
        name = self.hBox.drinkDetails.drinkName.text()
        caloriesPer100ml = (int)(self.hBox.drinkDetails.drinkAmount.text())
        self.controller.addNewDrink(name, caloriesPer100ml)
        self.changeToAddMealScreen()

