from src.App import *

class LoginScreen(QWidget):
    def __init__(self, parent, controller):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.layout = QVBoxLayout()
        self.setMaximumSize(1600, 14000)

        self.logoLabel = QLabel()
        self.logoLabel.pixmap = QPixmap("uiFiles/logo.png")
        self.logoLabel.pixmap = self.logoLabel.pixmap.scaled(1400, 1000, Qt.KeepAspectRatio)
        self.logoLabel.setPixmap(self.logoLabel.pixmap)
        self.layout.addWidget(self.logoLabel)

        self.usernameLabel = QLabel()
        self.usernameLabel.setText("Username:")
        self.layout.addWidget(self.usernameLabel)
        self.username = QLineEdit(self)
        self.layout.addWidget(self.username)

        self.passwordLabel = QLabel()
        self.passwordLabel.setText("Password:")
        self.layout.addWidget(self.passwordLabel)
        self.password = QLineEdit(self)
        self.password.setEchoMode(QLineEdit.Password)
        self.layout.addWidget(self.password)

        self.incorrectLoginLabelPlaceholder = QVBoxLayout()
        self.layout.addLayout(self.incorrectLoginLabelPlaceholder)
        self.incorrectLoginMade = False

        self.loginButton = QPushButton('Login')
        self.loginButton.clicked.connect(self.login)
        self.layout.addWidget(self.loginButton)
        self.incorrectLoginLabel = QLabel()

        self.newUserButton = QPushButton('New User')
        self.newUserButton.clicked.connect(self.newUser)
        self.layout.addWidget(self.newUserButton)

        self.setLayout(self.layout)

    def login(self):
        user = self.controller.sendLoginRequest(self.username.text(), self.password.text())
        if user == -1:
            self.incorrectLogin()
            return
        self.controller.setUserLoggedIn(user)
        self.controller.loadUserInfo()
        self.parent.setCurrentScreen("main")
        self.controller.checkGoalsExpire()

    def incorrectLogin(self):
        self.incorrectLoginLabel.setText("<font color='red'>Username " + self.username.text() + " or password incorrect.</font>")
        self.password.setText("")
        if not self.incorrectLoginMade:
            self.incorrectLoginMade = True
            self.incorrectLoginLabelPlaceholder.addWidget(self.incorrectLoginLabel)

    def newUser(self):
        self.parent.addToScreenStack("newUser")