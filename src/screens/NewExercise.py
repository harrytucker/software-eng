from src.App import *
from src.model.User import User
import datetime

#Screen on popup window to add a new exercise
class NewExercise(QWidget):
    def __init__(self, parent, controller):
        super().__init__()
        self.parent = parent
        self.controller = controller
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        #Title label
        self.title = QLabel()
        self.title.setText("Create new exercise")
        self.layout.addWidget(self.title)

        #Horizontal layout box
        self.hBox = QWidget()
        self.hBox.layout = QHBoxLayout()
        self.hBox.setLayout(self.hBox.layout)
        self.layout.addWidget(self.hBox)

        #Vertical layout box within horizontal box for exercise details
        self.hBox.exerciseDetails = QWidget()
        self.hBox.exerciseDetails.layout = QVBoxLayout()
        self.hBox.exerciseDetails.setLayout(self.hBox.exerciseDetails.layout)
        self.hBox.layout.addWidget(self.hBox.exerciseDetails)

        #Exercise title label and text entry
        self.hBox.exerciseDetails.exerciseTitleLabel = QLabel()
        self.hBox.exerciseDetails.exerciseTitleLabel.setText("Title for this exercise")
        self.hBox.exerciseDetails.layout.addWidget(self.hBox.exerciseDetails.exerciseTitleLabel)
        self.hBox.exerciseDetails.exerciseTitle = QLineEdit(self.hBox.exerciseDetails)
        self.hBox.exerciseDetails.layout.addWidget(self.hBox.exerciseDetails.exerciseTitle)

        #Exercise type label and drop down menu
        self.hBox.exerciseDetails.typeLabel = QLabel()
        self.hBox.exerciseDetails.typeLabel.setText("Exercise type")
        self.hBox.exerciseDetails.layout.addWidget(self.hBox.exerciseDetails.typeLabel)

        self.hBox.exerciseDetails.type = QComboBox()
        self.hBox.exerciseDetails.type.addItems(map(str, User.DEFAULT_USER_EXERCISES))
        self.hBox.exerciseDetails.type.currentIndexChanged.connect(self.selectType)
        self.hBox.exerciseDetails.layout.addWidget(self.hBox.exerciseDetails.type)
        self.exerciseType = 0
        self.currentExercise = None

        #Duration label and horizontal container box for text entries
        self.hBox.exerciseDetails.durationLabel = QLabel()
        self.hBox.exerciseDetails.durationLabel.setText("Duration")
        self.hBox.exerciseDetails.layout.addWidget(self.hBox.exerciseDetails.durationLabel)

        self.hBox.exerciseDetails.durationBox = QWidget()
        self.hBox.exerciseDetails.durationBox.layout = QHBoxLayout()
        self.hBox.exerciseDetails.durationBox.setLayout(self.hBox.exerciseDetails.durationBox.layout)
        self.hBox.exerciseDetails.layout.addWidget(self.hBox.exerciseDetails.durationBox)

        #Text entries for duration in hours:minutes:seconds order
        self.hBox.exerciseDetails.durationBox.hours = QLineEdit(self.hBox.exerciseDetails)
        self.hBox.exerciseDetails.durationBox.hours.setPlaceholderText("HH")
        self.hBox.exerciseDetails.durationBox.hours.setMaxLength(2)
        self.hBox.exerciseDetails.durationBox.hours.setMaximumWidth(44)
        self.hBox.exerciseDetails.durationBox.layout.addWidget(self.hBox.exerciseDetails.durationBox.hours)
        self.hBox.exerciseDetails.durationBox.layout.addWidget(QLabel(" :"))
        self.hBox.exerciseDetails.durationBox.minutes = QLineEdit(self.hBox.exerciseDetails)
        self.hBox.exerciseDetails.durationBox.minutes.setPlaceholderText("MM")
        self.hBox.exerciseDetails.durationBox.minutes.setMaxLength(2)
        self.hBox.exerciseDetails.durationBox.minutes.setMaximumWidth(44)
        self.hBox.exerciseDetails.durationBox.layout.addWidget(self.hBox.exerciseDetails.durationBox.minutes)
        self.hBox.exerciseDetails.durationBox.layout.addWidget(QLabel(" :"))
        self.hBox.exerciseDetails.durationBox.seconds = QLineEdit(self.hBox.exerciseDetails)
        self.hBox.exerciseDetails.durationBox.seconds.setPlaceholderText("SS")
        self.hBox.exerciseDetails.durationBox.seconds.setMaxLength(2)
        self.hBox.exerciseDetails.durationBox.seconds.setMaximumWidth(44)
        self.hBox.exerciseDetails.durationBox.layout.addWidget(self.hBox.exerciseDetails.durationBox.seconds)

        #Distance label and text box
        self.hBox.exerciseDetails.distanceAmountLabel = QLabel()
        self.hBox.exerciseDetails.distanceAmountLabel.setText("Distance")
        self.hBox.exerciseDetails.layout.addWidget(self.hBox.exerciseDetails.distanceAmountLabel)
        self.hBox.exerciseDetails.distanceAmount = QLineEdit(self.hBox.exerciseDetails)
        self.hBox.exerciseDetails.distanceAmount.setPlaceholderText("(M)")
        self.hBox.exerciseDetails.layout.addWidget(self.hBox.exerciseDetails.distanceAmount)

        #Vertical layout box within horizontal box for confirm and cancel buttons
        self.hBox.sideButtons = QWidget()
        self.hBox.sideButtons.layout = QVBoxLayout()
        self.hBox.sideButtons.setLayout(self.hBox.sideButtons.layout)
        self.hBox.layout.addWidget(self.hBox.sideButtons)

        #Add exercise button
        self.hBox.sideButtons.addButton = QPushButton("Add exercise")
        self.hBox.sideButtons.addButton.clicked.connect(self.submitNewExercise)
        self.hBox.sideButtons.layout.addWidget(self.hBox.sideButtons.addButton)

        #Cancel button
        self.hBox.sideButtons.cancelButton = QPushButton("Cancel")
        self.hBox.sideButtons.cancelButton.clicked.connect(self.cancel)
        self.hBox.sideButtons.layout.addWidget(self.hBox.sideButtons.cancelButton)

        #Error message for invalid inputs
        self.errorMessage = QLabel()
        self.layout.addWidget(self.errorMessage)

        #Initialise exercise type value
        self.selectType(0)

    #When user selects a type of exercise this method is invoked
    def selectType(self, i):
        self.exerciseType = i
        self.currentExercise = User.DEFAULT_USER_EXERCISES[i]

        #Hide or show duration and distance entry boxes if type of exercise uses or doesnt use them
        #e.g. tennis does not use distance but does use duration
        if self.currentExercise.hasDuration:
            self.hBox.exerciseDetails.durationLabel.show()
            self.hBox.exerciseDetails.durationBox.show()
        else:
            self.hBox.exerciseDetails.durationLabel.hide()
            self.hBox.exerciseDetails.durationBox.hide()

        if self.currentExercise.hasDistance:
            self.hBox.exerciseDetails.distanceAmountLabel.show()
            self.hBox.exerciseDetails.distanceAmount.show()
        else:
            self.hBox.exerciseDetails.distanceAmountLabel.hide()
            self.hBox.exerciseDetails.distanceAmount.hide()

    #Check user inputs to see if they are valid before sending request to create new exercise and then closing the popup
    def submitNewExercise(self):
        #Check title entry
        if self.hBox.exerciseDetails.exerciseTitle.text() == "":
            self.errorMessage.setText("<font color='red'>Please enter a title</font>")
            return

        #Check duration entry
        if self.currentExercise.hasDuration:
            if not self.hBox.exerciseDetails.durationBox.hours.text().isdigit():
                self.errorMessage.setText("<font color='red'>Hours must be a positive whole number</font>")
                return
            if not self.hBox.exerciseDetails.durationBox.minutes.text().isdigit():
                self.errorMessage.setText("<font color='red'>Minutes must be a positive whole number</font>")
                return
            elif (int)(self.hBox.exerciseDetails.durationBox.minutes.text()) > 60:
                self.errorMessage.setText("<font color='red'>Minutes cant be more than 60</font>")
                return
            if not self.hBox.exerciseDetails.durationBox.seconds.text().isdigit():
                self.errorMessage.setText("<font color='red'>Seconds must be a positive whole number</font>")
                return
            elif (int)(self.hBox.exerciseDetails.durationBox.seconds.text()) > 60:
                self.errorMessage.setText("<font color='red'>Seconds cant be more than 60</font>")
                return

        #Check distance entry
        if self.currentExercise.hasDistance:
            if not self.hBox.exerciseDetails.distanceAmount.text().isdigit():
                self.errorMessage.setText("<font color='red'>Distance must be a positive whole number</font>")
                return

        #Get exercise details to pass to controller
        distance = None
        duration = None

        dateAdded = datetime.date.today()
        now = datetime.datetime.now()
        timeAdded = datetime.time(now.hour, now.minute, now.second)
        if self.currentExercise.hasDistance:
            distance = (int)(self.hBox.exerciseDetails.distanceAmount.text())
        if self.currentExercise.hasDuration:
            hours = (int)(self.hBox.exerciseDetails.durationBox.hours.text())
            minutes = (int)(self.hBox.exerciseDetails.durationBox.minutes.text())
            seconds = (int)(self.hBox.exerciseDetails.durationBox.seconds.text())
            duration = datetime.time(hours, minutes, seconds)
        title = self.hBox.exerciseDetails.exerciseTitle.text()
        exerciseType = self.currentExercise.name

        #Pass details to controller and then close the popup
        self.parent.controller.addNewExercise(title, dateAdded, timeAdded, exerciseType, duration, distance)
        self.parent.confirmOnPopupClose("Exercise added")

    #Close the popup
    def cancel(self):
        self.parent.closePopup()