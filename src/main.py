from src.App import *

#Start point of program
if __name__ == "__main__":
    import sys
    app = Controller(sys.argv)
    sys.exit(app.app.exec_())

